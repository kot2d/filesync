/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filesync;


/**
 *
 * @author kot2d
 */
public class FileSync {

    /**
     * @param args the command line arguments
     */
    
    private static SourceTargetAnalayzer analayzer = new SourceTargetAnalayzer();
    private static String sourceDir;
    private static String destinationDir;

    
    public static void main(String[] args) throws Exception {
        
        try{
            sourceDir = args[0];
            destinationDir = args[1]; 
        }
        catch (Exception e){
            throw e;
        }
        
        FileMatcher fileMatcher = FileMatcher.INSTANCE;
        fileMatcher.setPatternString("^u_.+");
        //"C:\\MyData\\Art\\Desktop"
        analayzer.analyzeSource(sourceDir);
        //"C:\\MyData\\Art\\TestFolder"
        analayzer.analyzeTarget(destinationDir);
        analayzer.getDifference();
        analayzer.copyFromSourceToTarget();
    }
    
}
