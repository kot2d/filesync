/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filesync;

import java.io.File;
import java.nio.file.Path;
import java.util.Date;

/**
 *
 * @author Kahn
 */

public class DirectoryFile {
   
    public Path filePath;
    public String mainDirectoryRalativeFilePath;
    private Path mainDirectoryPath;
    private File file;
    private Date dateOfFileModification;
    
    public DirectoryFile(Path filePath){
        this.file = new File(filePath.toString());
        this.dateOfFileModification = new Date(this.file.lastModified()*1000L);
        this.filePath = filePath;
   }
   
   public long getModifiedtime(){
       return this.file.lastModified();
   }
   public String getFileName(){
       return file.getName();
   }
   
   public String getRelativeFileNameAndPath(){
       return mainDirectoryRalativeFilePath;
    }
   
   public DirectoryFile setMainDirectoryPath(Path directoryPath){
        this.mainDirectoryPath = directoryPath;
        int indexOfMaindirectory = this.mainDirectoryPath.getNameCount();
        this.mainDirectoryRalativeFilePath = file.toPath().subpath(indexOfMaindirectory,file.toPath().getNameCount()).toString();
        return this;
    }
   
   public boolean isSameFile(DirectoryFile targetFile){
       if(this.getRelativeFileNameAndPath().equals(targetFile.getRelativeFileNameAndPath())){
           if(this.getModifiedtime() == targetFile.getModifiedtime()){
               return true;
           }
       }
       return false;
   }
   
   public Path getFilePath(){
       return filePath;
   }
}
