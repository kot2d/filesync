/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filesync;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Kahn
 */

public class FileMatcher {
    
    private Pattern pattern;
    private Matcher matcher;
    public static final FileMatcher INSTANCE = new FileMatcher();
    private String patternString;
    
    public void setPatternString(String patternString){
        this.patternString = patternString;
        this.pattern = Pattern.compile(patternString);
     }
    
    public boolean checkFilename(String filename){
        try{
               matcher = pattern.matcher(filename);
               return matcher.matches();
            }
        catch (Exception e){
            throw e;        
        }
    }
}