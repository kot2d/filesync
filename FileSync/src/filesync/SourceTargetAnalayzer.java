/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filesync;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kahn
 */
public class SourceTargetAnalayzer {
    
    private DirectorySet source;
    private DirectorySet target;
    private List<DirectoryFile> sourceFileList;
    private List<DirectoryFile> targetFileList;
    private List<DirectoryFile> toReplaceFileList;
    
    public SourceTargetAnalayzer(){
        source = new DirectorySet();
        target = new DirectorySet();
        toReplaceFileList = new ArrayList<>();
    }
    
    
    public void analyzeSource(String path ){
        sourceFileList = source.anlyzeDirectory(path);
    }
    
    public void analyzeTarget(String path ){
        targetFileList = target.anlyzeDirectory(path);
    }
    
    public void getDifference(){
        boolean toReplace;
        
        if(!toReplaceFileList.isEmpty()){
            toReplaceFileList.clear();
        }
        
        for(DirectoryFile sourcefile : sourceFileList){
            toReplace = true;
            for(DirectoryFile targetFile : targetFileList){
                if(sourcefile.isSameFile(targetFile)){
                 toReplace = false;   
                }
            }
            if(toReplace){
                toReplaceFileList.add(sourcefile);
            }
        }
    }
    
    public void copyFromSourceToTarget() throws Exception{
        if(toReplaceFileList.isEmpty()){
            return;
        }
        try{
            for(DirectoryFile replaceFile : toReplaceFileList){
                Path pathToCopy = Paths.get(target.getPath().toString(), replaceFile.getRelativeFileNameAndPath());
                if(!Files.exists(pathToCopy.getParent())){
                    System.out.println(pathToCopy.toString());
                    Files.createDirectory(pathToCopy.getParent());
                }
                System.out.println(pathToCopy.toString());
                Files.copy(replaceFile.getFilePath(), pathToCopy, StandardCopyOption.REPLACE_EXISTING);
            }
        }
        catch(Exception e){
            throw e;
        }
    }
}