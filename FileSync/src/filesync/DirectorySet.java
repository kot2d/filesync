/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filesync;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kot2d
 */
public class DirectorySet {
    
    private Path directoryPath;
    private List<DirectoryFile> fileList;
    private FileMatcher fileMatcher;

    public DirectorySet(){
        //this.directoryPath = Paths.get(path);        
        fileMatcher = FileMatcher.INSTANCE;
        fileList = new ArrayList<>();
    }
    
    private void collectFilesFromPath(Path directory){
        if(!fileList.isEmpty()){
            fileList.clear();
        }
        try(DirectoryStream<Path> stream = Files.newDirectoryStream(directory)){
            for(Path path : stream){
                if(path.toFile().isDirectory()){
                    collectFilesFromPath(path);
                }
                else {
                    DirectoryFile file = new DirectoryFile(path).setMainDirectoryPath(directoryPath);
                    if(fileMatcher.checkFilename(file.getFileName())){
                        fileList.add(file);
                    }
                }
            }
        } catch(IOException e) {
        e.printStackTrace();
        }
     }
    
    public List<DirectoryFile> anlyzeDirectory(String path){
        this.directoryPath = Paths.get(path);
        collectFilesFromPath(directoryPath);
        return fileList;
    }
    public Path getPath(){
        return this.directoryPath;
    }
}